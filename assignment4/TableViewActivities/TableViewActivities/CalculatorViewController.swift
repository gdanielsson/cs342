//
//  CalculatorViewController.swift
//  TableViewActivities
//
//  Created by Alex Calamaro on 4/21/15.
//  Copyright (c) 2015 Gustav. All rights reserved.
//

import Foundation
import UIKit

class CalculatorViewController: UIViewController {
    
    
    @IBOutlet weak var screen: UILabel!
    
    @IBOutlet weak var clear: UIButton!
    @IBOutlet weak var equals: UIButton!
    @IBOutlet weak var plus: UIButton!
    @IBOutlet weak var minus: UIButton!
    @IBOutlet weak var multiply: UIButton!
    @IBOutlet weak var divide: UIButton!
    
    @IBOutlet weak var dot: UIButton!
    @IBOutlet weak var zero: UIButton!
    @IBOutlet weak var one: UIButton!
    @IBOutlet weak var two: UIButton!
    @IBOutlet weak var three: UIButton!
    @IBOutlet weak var four: UIButton!
    @IBOutlet weak var five: UIButton!
    @IBOutlet weak var six: UIButton!
    @IBOutlet weak var seven: UIButton!
    @IBOutlet weak var eight: UIButton!
    @IBOutlet weak var nine: UIButton!
    
    var accumulator : String = ""
    var storage : String = ""
    var nextOp : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        screen.text = accumulator
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clearpress(sender: AnyObject) {
        accumulator = ""
        screen.text = accumulator
        storage = ""
        if(!nextOp.isEmpty){
            nextOp = ""
        }
    }
    @IBAction func equalspress(sender: AnyObject) {
        
        if !(self.accumulator.isEmpty || self.storage.isEmpty ){
            var argOne = (storage as NSString).floatValue
            var argTwo = (accumulator as NSString).floatValue
            
            if(nextOp == "plus"){accumulator = (argOne + argTwo).description}
            else if(nextOp == "minus"){accumulator = (argOne - argTwo).description}
            else if(nextOp == "mult"){accumulator = (argOne * argTwo).description}
            else if(nextOp == "div"){accumulator = (argOne / argTwo).description}
            
            screen.text = accumulator
            storage = ""
            nextOp = ""
        }
    }
    @IBAction func pluspress(sender: AnyObject) {
        if(!accumulator.isEmpty){
            storage = accumulator
            accumulator = ""
            nextOp = "plus"
        }
    }
    
    @IBAction func minuspress(sender: AnyObject) {
        if(!accumulator.isEmpty){
            storage = accumulator
            accumulator = ""
            nextOp = "minus"
        }
    }
    @IBAction func multpress(sender: AnyObject) {
        if(!accumulator.isEmpty){
            storage = accumulator
            accumulator = ""
            nextOp = "mult"
        }
    }
    @IBAction func divpress(sender: AnyObject) {
        if(!accumulator.isEmpty){
            storage = accumulator
            accumulator = ""
            nextOp = "div"
        }
    }
    
    @IBAction func zeropress(sender: AnyObject) {
        accumulator += "0"
        screen.text = accumulator
    }
    
    @IBAction func onepress(sender: AnyObject) {
        accumulator += "1"
        screen.text = accumulator
    }
    @IBAction func twopress(sender: AnyObject) {
        accumulator += "2"
        screen.text = accumulator
    }
    @IBAction func threepress(sender: AnyObject) {
        accumulator += "3"
        screen.text = accumulator
    }
    @IBAction func fourpress(sender: AnyObject) {
        accumulator += "4"
        screen.text = accumulator
    }
    @IBAction func fivepress(sender: AnyObject) {
        accumulator += "5"
        screen.text = accumulator
    }
    
    @IBAction func sixpress(sender: AnyObject) {
        accumulator += "6"
        screen.text = accumulator
    }
    
    @IBAction func sevenpress(sender: AnyObject) {
        accumulator += "7"
        screen.text = accumulator
    }
    @IBAction func eightpress(sender: AnyObject) {
        accumulator += "8"
        screen.text = accumulator
    }
    @IBAction func ninepress(sender: AnyObject) {
        accumulator += "9"
        screen.text = accumulator
    }
    @IBAction func dotpress(sender: AnyObject) {
        accumulator += "."
        screen.text = accumulator
    }
}
