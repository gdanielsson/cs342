    //
//  MapViewController.swift
//  TableViewActivities
//
//  Created by Gustav on 4/21/15.
//  Copyright (c) 2015 Gustav. All rights reserved.
//
//  Simple map activity that shows Northfield and has a pin where Carleton is
//
    
import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var myMap: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let location = CLLocationCoordinate2D(
            latitude: 44.461944,
            longitude: -93.153778
        )
        
        let span = MKCoordinateSpanMake(0.01, 0.01)
        let region = MKCoordinateRegion(center: location, span: span)
        myMap.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Carleton College"
        myMap.addAnnotation(annotation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}
