//
//  ViewController.swift
//  tableView
//
//  Created by Gustav Danielsson and Alex Calamaro
//

import UIKit

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var About: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!
    
    var items = ["Wikipedia", "Carleton Map", "Calculator"]
    let cellIdentifier = "Activities"
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        title = "Activities"
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return indexPath
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            
        }
        
        cell.textLabel?.text = self.items[indexPath.row]
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index = indexPath.row
        if(index == 0){
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("Wikipedia") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(index == 1){
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("Map") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(index == 2){
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("Calculator") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
}

